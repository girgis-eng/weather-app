import { Component, OnInit, Output, EventEmitter, Input, HostListener, ElementRef } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() searchCity: EventEmitter<any> = new EventEmitter;
  @Input("hasSearch") hasSearch: boolean;
  showCities = false;
  showSearch = true;
  cities: any[];
  currentCity = {};

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showCities = false;
      this.showSearch = false;
    }
  }

  constructor(
    private weatherService: WeatherService,
    private eRef: ElementRef
  ) { }

  ngOnInit() {
    this.getCitiesList();
  }

  getCitiesList() {
    this.weatherService.citiesList().subscribe(res => {
      this.cities = res;
    })
  }

  setCurrentCity(city) {
    this.showCities = false;
    this.showSearch = false;
    this.currentCity = city;
    this.searchCity.emit(city);
  }

}
