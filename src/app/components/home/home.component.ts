import { Component, OnInit, OnDestroy } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  // default city is Jiza
  currentCity = {
    id: 360995
  };
  loading = true;
  weatherData: object;
  hourlyForecast: object;
  updatedIn: string;
  sunTimes = {
    sunrise: "",
    sunset: ""
  };
  refreshIntervalId;
  err = { msg: "" };
  tempToBeFormatted = ["temp", "feels_like", "temp_min", "temp_max"];
  subscription;
  constructor(private weatherService: WeatherService) { }
  
  ngOnInit() {
    this.cityDataById();
  }
  
  checkIfSunset() {
    let now = (new Date()).getHours();
    let citySunset = parseInt(this.sunTimes.sunset.split(":")[0])
    if (now > citySunset) return true;
    return false;
  }

  cityDataById() {
    this.loading = true;
    this.subscription = this.weatherService.weatherData("id", this.currentCity.id).subscribe(res => {
      this.weatherData = res;
      this.reformatTemp();
      this.sunTimes['sunrise'] = this.unixTimeToHours(this.weatherData['sys'].sunrise);
      this.sunTimes['sunset'] = this.unixTimeToHours(this.weatherData['sys'].sunset);
      clearInterval(this.refreshIntervalId);
      this.watchDataFetchTime();
      this.loading = false;
    }, err => {
      this.handleReqErr(err);
    })
  }

  watchDataFetchTime() {
    let dataFetchedSince = 0;
    this.whenDataWasUpdated(dataFetchedSince);
    this.refreshIntervalId = setInterval(() => {
      dataFetchedSince = this.whenDataWasUpdated(dataFetchedSince);
    }, 15000);
  }

  whenDataWasUpdated(seconds) {
    seconds += 15;
    if (seconds < 60) this.updatedIn = "a few seconds ago";
    else if (seconds >= 60 && seconds < 3600) {
      let minutes = Math.floor(seconds / 60);
      this.updatedIn = `${minutes} minutes ago`;
    } else {
      let hours = Math.floor(seconds / 3600);
      this.updatedIn = `${hours} hours ago`;
    }
    return seconds;
  }

  unixTimeToHours(unixTime) {
    let dateObj = new Date(unixTime * 1000); 
    // Get hours from the timestamp 
    let hours = dateObj.getHours(); 
    // Get minutes part from the timestamp 
    let minutes = dateObj.getUTCMinutes(); 
    let formattedTime = hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0'); 
    return formattedTime;
  }

  reformatTemp() {
    this.tempToBeFormatted.map(key => {
      let mainTempratures = this.weatherData['main'];
      // convert from kelvin to Celsius by subtracting 273.15
      if (mainTempratures[key]) mainTempratures[key] = Math.round(mainTempratures[key] - 273.15);
    })
  }

  handleReqErr(err) {
    this.loading = false;
    this.err = { msg: err };
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
