import { Component, OnInit, OnDestroy } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit, OnDestroy {
  loading = true;
  cityId: string;
  weatherData: object;
  updatedIn: string;
  refreshIntervalId;
  err = { msg: "" };
  subscription;
  constructor(
    private weatherService: WeatherService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.cityId = this.route.snapshot.params.id;
    this.cityDataById();
  }

  cityDataById() {
    this.loading = true;
    this.subscription = this.weatherService.forecastData("id", this.cityId).subscribe(res => {
      this.weatherData = res;
      this.reformatResponse();
      clearInterval(this.refreshIntervalId);
      this.watchDataFetchTime();
      this.loading = false;
    }, err => {
      this.handleReqErr(err);
    })
  }

  watchDataFetchTime() {
    let dataFetchedSince = 0;
    this.whenDataWasUpdated(dataFetchedSince);
    this.refreshIntervalId = setInterval(() => {
      dataFetchedSince = this.whenDataWasUpdated(dataFetchedSince);
    }, 15000);
  }

  whenDataWasUpdated(seconds) {
    seconds += 15;
    if (seconds < 60) this.updatedIn = "a few seconds ago";
    else if (seconds >= 60 && seconds < 3600) {
      let minutes = Math.floor(seconds / 60);
      this.updatedIn = `${minutes} minutes ago`;
    } else {
      let hours = Math.floor(seconds / 3600);
      this.updatedIn = `${hours} hours ago`;
    }
    return seconds;
  }

  reformatResponse() {
    this.weatherData['list'] = this.weatherData['list'].map(obj => {
      // convert from kelvin to Celsius by subtracting 273.15
      obj.weather = obj.weather[0];
      obj.weather.temp = Math.round(obj['main'].temp - 273.15);
      obj.weather['dt_txt'] = obj['dt_txt'];
      return obj.weather;
    })
  }

  handleReqErr(err) {
    this.loading = false;
    this.err = { msg: err };
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
