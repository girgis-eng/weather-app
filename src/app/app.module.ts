import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// routing modules
import { AppRoutingModule } from './app-routing.module';

// components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ForecastComponent } from './components/forecast/forecast.component';

// common components
import { NavbarComponent } from './shared/navbar/navbar.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ErrorComponent } from './shared/error/error.component';

// service
import { WeatherService } from './services/weather.service';


@NgModule({
  declarations: [
    // components
    AppComponent,
    HomeComponent,
    ForecastComponent,

    // common components
    NavbarComponent,
    LoadingComponent,
    FilterPipe,
    ErrorComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
