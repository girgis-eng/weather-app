import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(
    private http: HttpClient
  ) { }

  private urlBuilder(urlData) {
    let apiData = environment.api;
    return `
      ${apiData.url}/${urlData.type}?${urlData.using}=${urlData.data}&appid=${apiData.key}
    `
  }
  
  citiesList(): Observable<any> {
    return this.http.get("/assets/city.list.json");
  }

  weatherData(using, data): Observable<any> {
    let urlData = {
      using,
      data,
      type: "weather",
    };
    let requestUrl = this.urlBuilder(urlData).trim();
    return this.http.get(requestUrl);
  }

  forecastData(using, data): Observable<any> {
    let urlData = {
      using,
      data,
      type: "forecast",
    };
    let requestUrl = this.urlBuilder(urlData).trim();
    return this.http.get(requestUrl);
  }
}
